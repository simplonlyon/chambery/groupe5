import React, { useEffect, useState } from "react";
import {getPersons} from "../API";
import ModalSelected from "../components/ModalSelected";
import PersonsList from "../components/PersonsList";


export default function Home() {
    const [persons, setPersons] = useState([]);
    const [selected, setSelected] = useState(null);

    useEffect(() => {
        getPersons().then(data => setPersons(data));
    }, []);

    
    return (
        <section>
            <h1>Persons List</h1>
            <PersonsList persons={persons} setSelected={setSelected} />
            
            {selected && 
                <ModalSelected selected={selected} setSelected={setSelected} />
            }
        </section>
    )
}