import React from "react";

export default function({persons, setSelected}) {
    return <div className="list-group">
        {persons.map(person => 
            <button key={person.id} onClick={() => setSelected(person)} className="list-group-item">{person.name} {person.firstname}</button>
        )}
    </div>
}
